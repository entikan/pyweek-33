from panda3d.core import CollideMask
from panda3d.core import DirectionalLight
from .levels import Level
from .player import Player


class Sound():
    def __init__(self):
        self.sfx = {}
        for i in ["death", "death2","explosion1", "explosion2", "explosion3", "laser_o", "laser_b", "respawn", 'rubble1', 'rubble2', 'rubble3']:
            self.sfx[i] = loader.load_sfx('assets/sfx/{}.wav'.format(i))
        self.music = {}
        for i in ["lom0","lom1","lom2","lom3","lom4"]:
            self.music[i] = loader.load_sfx('assets/music/{}.ogg'.format(i))
            self.music[i].set_loop(True)
        self.now_playing = ''

    def play_song(self, i):
        if not self.now_playing == i:
            self.now_playing = i
            for song in self.music:
                self.music[song].stop()
            self.music[i].play()


class Game():
    def __init__(self):
        base.cam.set_pos(9999,9999,-9999)
        self.sound = Sound()
        self.player_o = Player()
        self.player_b = Player(self.player_o)
        self.players = [self.player_o, self.player_b]

        w = 0.02
        split = render2d.attach_new_node(base.cardmaker.generate())
        split.set_scale(w,2,4)
        split.set_pos(-w/2,0,-1)
        split.set_color(0,0,0)