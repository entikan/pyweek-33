from panda3d.core import (
    NodePath,
    CollisionNode,
    CollideMask,
    CollisionSphere,
    CollisionSegment,
    CollisionHandlerPusher,
    CollisionHandlerQueue,
    CollisionTraverser,
)
from direct.actor.Actor import Actor


class Creature():
    def __init__(self, name, model):
        self.name = name
        self.hp = 1
        self.accel= 1
        self.alive = True
        self.speed= NodePath('speed')
        self.target = NodePath('aim')
        self.model = Actor(model)
        self.root = render.attach_new_node(name)
        self.model.reparent_to(self.root)
        self.setup_collision()
        base.task_mgr.add(self.update)

    def update(self, task):
        self.move()

    def move(self, x=0.0, y=0.0):
        if not base.game.level.is_ending:
            self.speed.set_pos(self.speed, (x, y, 0))
            self.model.look_at(self.speed.get_pos())
            self.speed.set_pos(self.speed.get_pos()*0.9)
            self.root.set_pos(self.root, self.speed.get_pos())

    def setup_collision(self):
        m = {'o':1, 'b':0}[self.color]
        collision = self.root.attach_new_node(CollisionNode('body_'+self.name))
        collision.node().add_solid(CollisionSphere(center=(0,0,1), radius=1))
        collision.node().set_from_collide_mask(CollideMask.bit(1))
        collision.node().set_into_collide_mask(CollideMask.bit(4-m))
        handler = CollisionHandlerPusher()
        handler.horizontal = True
        handler.add_collider(collision, self.root)
        base.cTrav.add_collider(collision, handler)
        body = collision


        self.traverser = CollisionTraverser()
        collision = self.root.attach_new_node(CollisionNode('seeing_'+self.name))
        self.segment = CollisionSegment((0,0,1), (0,30,1))
        collision.node().add_solid(self.segment)
        collision.node().set_from_collide_mask(CollideMask.bit(3+m))
        collision.node().set_into_collide_mask(0)
        self.collision=collision
        #self.collision.show()

        self.queue = CollisionHandlerQueue()
        self.traverser.add_collider(collision, self.queue)
        if hasattr(self, 'twin'):
            collision = self.root.attach_new_node(CollisionNode('exit'))
            collision.node().add_solid(CollisionSphere(center=(0,0,0), radius=2))
            collision.node().set_from_collide_mask(CollideMask.bit(8+m))
            collision.node().set_into_collide_mask(CollideMask.bit(8+m))
            self.exit_handler = CollisionHandlerQueue()
            base.cTrav.add_collider(collision, self.exit_handler)
        else:
            self.segment.set_point_a((0,1.5,1))
            self.collision.node().set_from_collide_mask(CollideMask.bit(1)|CollideMask.bit(2)|CollideMask.bit(3)|CollideMask.bit(4))


    def look_towards(self, target_pos, offset=0):
        self.target.set_pos(target_pos)
        if self.root.get_distance(self.target) > 40:
            return
        self.collision.look_at(self.target)
        self.collision.set_h(self.collision, offset)
        self.traverser.traverse(render)
        self.queue.sort_entries()
        if len(self.queue.entries) > 0:
            return self.queue.entries[0]