from panda3d.core import CollideMask, TextNode
from .enemies import Enemy
from .common import COLORS

class Level():
    def __init__(self, o, b):
        self.ln = -1
        self.root = render.attach_new_node('root')
        self.o, self.b = o, b
        self.cutscene = 0
        self.endscene = 0
        self.spawned = False
        self.is_ending = False
        base.task_mgr.add(self.update)

    def next_level(self):
        self.ln += 1
        self.root.detach_node()
        self.root = render.attach_new_node('root')
        self.level = loader.load_model('assets/bam/maps/a{}.bam'.format(self.ln))
        self.level.reparent_to(self.root)
        self.is_in = 0

        self.spawn_o = self.level.find('**/spawn_o')
        self.spawn_b = self.level.find('**/spawn_b')
        self.exit_o = self.level.find('**/exit_o')
        self.exit_o.set_color(COLORS['b'], 1)
        self.exit_o.set_collide_mask(CollideMask.bit(8))
        self.exit_b = self.level.find('**/exit_b')
        self.exit_b.set_color(COLORS['o'], 1)
        self.exit_b.set_collide_mask(CollideMask.bit(9))
        self.cutscenes = list(self.level.find_all_matches('**/cutscene*'))

        if self.ln == 9:
            self.is_ending = True
            base.task_mgr.add(self.ends)

        self.enemies = []
        levels = 8
        songs = 4
        song = int((songs/levels)*(self.ln))+1
        if not song >= 5:
            base.game.sound.play_song('lom{}'.format(song))

        mask = CollideMask.bit(0)|CollideMask.bit(1)|CollideMask.bit(2)|CollideMask.bit(3)|CollideMask.bit(4)
        self.level.find('**/walls').set_collide_mask(mask)
        self.restart()

    def ends(self, task):
        if self.o.cuttimer <= 0:
            self.o.run_cutscene(self.endscene, False)
            self.b.run_cutscene(self.endscene, False)
            self.endscene += 1
        if self.endscene > 4:
            text = render2d.attach_new_node(TextNode('ending'))
            text.node().set_align(2)
            text.node().text = 'THE END\nThanks for playing!\n\n\nMade by Entikan\nfor pyweek-33'
            text.set_scale(0.1)
            self.is_ending = False
            base.game.sound.play_song('lom0')
            return task.done
        return task.cont

    def update(self, task):
        if self.is_in >= 2:
            self.next_level()

        for cutscene in self.cutscenes:
            if cutscene.get_distance(self.o.root) < cutscene.get_scale().x or cutscene.get_distance(self.b.root) < cutscene.get_scale().x:
                self.cutscenes.remove(cutscene)
                self.o.run_cutscene(self.cutscene)
                self.b.run_cutscene(self.cutscene)
                self.cutscene += 1
                return task.cont

        return task.cont

    def restart(self):
        self.spawned = True
        base.game.sound.sfx['respawn'].play()
        self.o.respawn(self.spawn_o.get_pos(render))
        self.o.root.set_z(0)
        self.b.respawn(self.spawn_b.get_pos(render))
        self.b.root.set_z(0)
        for enemy in self.enemies:
            enemy.remove()

        self.enemies = []
        for enemy in self.level.find_all_matches('**/enemy_*'):
            name = enemy.name.split('.')[0]
            model_name, color = name.split('_')[1:]
            pos = enemy.get_pos(render)
            pos.z = 0
            self.enemies.append(Enemy(pos, 'assets/bam/enemies/{}.bam'.format(model_name), color, enemy.name))
