from random import randint
from .common import COLORS
from .creature import Creature


class Explosion():
    def __init__(self, pos):
        self.root = loader.load_model('assets/bam/explosion.bam')
        self.root.reparent_to(render)
        self.root.set_pos(render, pos)
        self.root.set_transparency(True)
        self.time = 2
        self.d = True
        base.task_mgr.add(self.update)

    def update(self, task):
        self.time -= base.clock.get_dt()
        if self.time <= 1.9 and self.d:
            self.d = False
            boom = base.game.sound.sfx['explosion{}'.format(randint(1,3))]
            boom.set_volume(0.5)
            boom.play()
            rub = base.game.sound.sfx['rubble{}'.format(randint(1,3))]
            rub.set_volume(1.5)
            rub.play()

        if self.time <= 0:
            self.root.detach_node()
            return task.done
        self.root.set_scale(self.root, 1+(self.time*base.clock.get_dt()))
        self.root.set_alpha_scale(self.time/2)
        self.root.set_h(self.root, 5)
        return task.cont


class Enemy(Creature):
    def __init__(self, pos, model, color, debug_name):
        self.debug_name = debug_name
        self.color = {'o':'b','b':'o'}[color]
        super().__init__('enemy', model)
        self.color = color
        self.last_seen = None
        self.root.set_pos(pos)
        self.model.loop('move')

        for colorize in self.model.find_all_matches('**/*colorize*'):
            colorize.set_color(COLORS[self.color])
        self.hp = 1
        self.go_away = False

    def remove(self):
        self.root.detach_node()
        self.go_away = True

    def hit(self, color, who):
        if color == self.color:
            self.hp -= 1
            if self.hp <= 0:
                Explosion(self.root.get_pos())
                self.root.detach_node()
                self.go_away = True

    def look(self):
        base.game.players.sort(key=lambda d: d.root.get_distance(self.root), reverse=True)
        for player in base.game.players:
            if player.alive:
                seeing = self.look_towards(player.root.get_pos(render))
                if seeing:
                    np = seeing.get_into_node_path()
                    if np.name == 'body_'+player.name:
                        self.last_seen = player.root.get_pos()
                        if player.color == self.color:
                            player.enemies_spotted.append(self)
                        return

    def update(self, task):
        if self.go_away:
            return task.done
        if self.alive:
            self.look()
            if self.last_seen:
                self.root.look_at(self.last_seen)
                self.move(0,1.5*base.clock.get_dt())
            for player in base.game.players:
                if self.root.get_distance(player.root) < 2:
                    player.hit('o', self)
        return task.cont
