from random import uniform, gauss
from panda3d.core import Camera, BitMask32, SequenceNode, TextNode
from direct.actor.Actor import Actor
from .creature import Creature
from .common import COLORS


cutscenes_sad = [
    "you need to come save me!",
    "your evil twin kidnapped me!",
    "be careful!",
    "your evil twin hacked the security system!",
    "watch out it's after you aaaaah!",
    "your evil twin is after you!",
    "your evil twin can't be trusted!",
    "please save me from your evil twin!",
    "your evil twin wants to hurt me!",
    "heeeelp",
    "your evil twin wants you dead!",
    "noooooo!!!",
    "come quickly!!!",
    "he will stop at nothing!",
    "aaaaaaahahaha!!",
]
cutscenes_angry = [
    "very good twins",
    "this is the end for you",
    "now you will be stuck here",
    "forever",
    'hahahaha'
]


class BulletFly():
    def __init__(self, owner, target, color):
        self.root = loader.load_model('assets/bam/bullet.bam')
        self.root.reparent_to(render)
        self.root.set_color(color)
        self.root.set_light_off()
        self.root.set_pos(render, owner.get_pos(render))
        self.root.look_at(target)
        self.root.set_p(0)
        self.root.set_r(0)
        self.root.set_y(self.root, 1)
        self.root.set_z(1)
        self.speed = 100
        base.task_mgr.add(self.update)

    def update(self, task):
        self.root.set_fluid_y(self.root, self.speed*base.clock.get_dt())
        def hit(creature):
            player.model.set_h(self.root.get_h())
            player.root.set_pos(player.model, 0.2)
            self.root.detach_node()

        for player in base.game.players:
            if self.root.get_distance(player.root) <= 2:
                hit(player)
                return task.done
        if len(base.game.enemies) > 0:
            base.game.enemies.sort(key=lambda d: d.root.get_distance(self.root), reverse=True)
            enemy = base.game.enemies[0]
            if self.root.get_distance(enemy.root) <= 2:
                hit(enemy)
                return task.done
        return task.cont


class BulletScan():
    def __init__(self, a, b, color):
        a.z = 1; b.z = 1
        base.linesegs.set_color((1,1,1,1))
        base.linesegs.move_to(a)
        base.linesegs.draw_to(a)
        base.linesegs.set_color(color)
        base.linesegs.draw_to(b)
        self.root = render.attach_new_node(base.linesegs.create())
        self.root.set_light_off()
        self.root.set_transparency(True)
        self.scale = 1
        base.task_mgr.add(self.update)

    def update(self, task):
        self.scale -= base.clock.get_dt()
        self.root.set_alpha_scale(self.scale)
        if self.scale <= 0:
            self.root.detach_node()
            return task.done
        return task.cont


class Weapon():
    def __init__(self, owner, color):
        self.owner = owner
        self.color = color
        self.offset = 0.2
        self.cooldown = [uniform(0,self.offset),1]
        self.reload_time = [0,0]
        base.task_mgr.add(self.update)

    def update(self, task):
        self.cooldown[0] -= base.clock.get_dt()
        return task.cont

    def trigger(self, target, entity=None):
        if self.cooldown[0] < 0:
            self.cooldown[0] = self.cooldown[1]+uniform(0,self.offset)
            origin = self.owner.root.get_pos(render)
            if self.owner.twin:
                origin+=(0.2,0.2,0)
            else:
                origin-=(0.2,0.2,0)
            base.game.sound.sfx['laser_{}'.format(self.color)].play()
            BulletScan(origin, target.get_pos(render), COLORS[self.color])
            if entity:
                entity.hit(self.color, self.owner)
            self.reload_time[0] = 0


class Player(Creature):
    def __init__(self, brother=None):
        if brother:
            self.brother = brother
            brother.brother = self
        self.twin = 1 if brother else 0
        self.cuttimer = 0
        self.color = ('b','o')[self.twin]
        super().__init__('player_{}'.format(self.twin),'assets/bam/player_top.bam')
        self.legs = Actor('assets/bam/player_legs.bam')
        self.legs.reparent_to(self.root)
        self.model.set_play_rate(2, 'walk')
        self.legs.set_play_rate(2, 'walk')
        self.model.find('**/hair').set_color(COLORS[self.color])
        self.weapon = Weapon(self, self.color)
        self.setup_region()
        self.wait = [0,0.3]
        self.enemies_spotted = []
        self.seen = None
        self.body = None
        self.target_breath = 0
        self.accel = 2

    def setup_region(self):
        self.camera = self.root.attach_new_node(Camera('twin_{}'.format(self.twin)))
        self.camera.set_z(100)
        self.camera.look_at(self.root)
        self.camera.node().set_camera_mask(BitMask32.bit(self.twin))
        l, r = (self.twin/2), 0.5+(self.twin/2)
        self.region = base.win.make_display_region(l, r, 0, 1)
        self.region.set_camera(self.camera)

        self.fade_card = self.camera.attach_new_node(base.cardmaker.generate())
        self.fade_card.set_scale(20)
        self.fade_card.set_y(1+self.twin)
        self.fade_card.set_x(-1)
        self.fade_card.set_z(-1)
        self.fade_card.set_transparency(True)
        self.fade_card.set_alpha_scale(0)
        self.fade_card.hide(BitMask32.bit([1,0][self.twin]))
        self.fade_card.hide()
        self.setup_cutscene()

    def setup_cutscene(self):
        self.cutscene = self.camera.attach_new_node('cutscene')
        self.cutscene.hide(BitMask32.bit([1,0][self.twin]))
        self.cutscene.set_y(2)
        self.cutscene.set_scale(0.1)
        self.cutscene_text = self.cutscene.attach_new_node(TextNode('cutscene'))
        self.cutscene_text.node().align = 2
        self.cutscene_text.set_scale(0.3)
        self.cutscene_text.set_z(-1)
        self.cutscene_text.set_y(-0.2)
        kwien = loader.load_model('assets/bam/kwien.bam')
        self.kwien_sad = self.cutscene.attach_new_node(SequenceNode('kwien_sad'))
        self.kwien_angry = self.cutscene.attach_new_node(SequenceNode('kwien_angry'))
        kwien.find("kwien_sad").reparent_to(self.kwien_sad)
        kwien.find("kwien_sad_o").reparent_to(self.kwien_sad)
        kwien.find("kwien_angry").reparent_to(self.kwien_angry)
        kwien.find("kwien_angry_o").reparent_to(self.kwien_angry)
        self.kwien_sad.node().frame_rate = 4+self.twin
        self.kwien_sad.node().loop(1)
        self.kwien_angry.node().frame_rate = 3
        self.kwien_angry.node().loop(1)
        self.kwien_angry.hide()

        self.kwien_sad.hide()

    def run_cutscene(self, i, sad=True):
        if sad:
            self.kwien_sad.show()
            self.cutscene_text.node().text = cutscenes_sad[i]
        else:
            self.kwien_angry.show()
            self.cutscene_text.node().text = cutscenes_angry[i]
        self.cuttimer = 3
        base.task_mgr.add(self.update_cutscene)

    def update_cutscene(self, task):
        self.cuttimer -= base.clock.get_dt()
        if self.cuttimer <= 0:
            self.cutscene_text.node().text = ''
            self.kwien_sad.hide()
            self.kwien_angry.hide()
            return task.done
        return task.cont

    def respawn(self, pos):
        self.d = True
        self.hp = 1
        self.alive = True
        if self.body:
            self.body.detach_node()
        self.model.show()
        self.legs.show()
        self.root.set_pos(pos)
        self.fade_card.set_color((1,1,1,1))
        #self.fade_card.set_alpha_scale(1)
        self.fade = 1
        self.fade_card.show()
        self.fade_card.hide(BitMask32.bit([1,0][self.twin]))
        base.task_mgr.add(self.fade_in)

    def fade_in(self, task):
        self.fade -= base.clock.get_dt()
        if self.fade <= 0:
            self.fade_card.hide()
            return task.done
        self.fade_card.set_alpha_scale(self.fade)
        return task.cont

    def death(self, task):
        self.death_time -= base.clock.get_dt()
        if self.alive:
            return task.done
        if self.death_time < 0:
            base.game.level.restart()
            self.fade_card.set_alpha_scale(0)
            #self.fade_card.hide()
            return task.done
        if self.death_time < 1.5:
            if self.d:
                base.game.sound.sfx['death2'].play()
                self.d = False
            self.fade_card.set_color((1,0,0,1))
            self.fade_card.set_alpha_scale((1.5-self.death_time)/1.5)
        return task.cont

    def animate(self):
        if self.speed.get_pos().length() > 0.1:
            if not self.model.get_current_anim() == 'walk':
                self.model.loop('walk')
                self.legs.loop('walk')
        else:
            self.model.loop('idle')
            self.legs.loop('idle')

    def fire(self):
        if self.seen:
            self.model.look_at(self.seen[0].root)
            self.target_breath -= base.clock.get_dt()
            if self.target_breath <= 0:
                target_offset = gauss(0,1)
                seeing = self.look_towards(self.seen[1], target_offset)
                if seeing:
                    self.target.set_pos(seeing.get_surface_point(render))
                    if seeing.get_into_node_path().name.startswith('body_'):
                        self.weapon.trigger(self.target, self.seen[0])
                    else:
                        self.weapon.trigger(self.target)
                self.seen = None
            return True

    def aim(self):
        if self.brother.alive:
            seen = self.look_towards(self.brother.root.get_pos())
            if seen:
                if seen.get_into_node_path().name == 'body_'+self.brother.name:
                    self.seen = self.brother, self.brother.root.get_pos()
        if len(self.enemies_spotted) > 0:
            self.enemies_spotted.sort(key=lambda d: d.root.get_distance(self.root), reverse=True)
            enemy = self.enemies_spotted[0]
            if not self.seen or (self.seen and enemy.root.get_distance(self.root) < self.seen[0].root.get_distance(self.root)):
                self.seen = enemy, enemy.root.get_pos(render)
        if self.seen:
            self.target_breath = uniform(0.1, 0.4) #TODO: skill based?
        self.enemies_spotted = []

    def update(self, task):
        if not self.alive or base.game.level.is_ending:
            return task.cont
        self.weapon.reload_time[0] += base.clock.get_dt()
        context = base.device_listener.read_context('player_{}'.format(self.twin))
        x, y = context['move'].x*self.accel, context['move'].y*self.accel
        self.legs.look_at(self.speed.get_pos())
        self.move(x*base.clock.get_dt(), y*base.clock.get_dt())
        if not self.fire():
            self.aim()
        self.animate()

        if len(self.exit_handler.entries) > 0:
            np = self.exit_handler.entries[0].get_into_node_path()
            base.game.level.is_in += 1
        else:
            base.game.level.is_in = 0
        return task.cont

    def hit(self, color, d):
        self.hp -= 1
        if self.hp <= 0 and self.alive:
            base.game.sound.sfx['death'].play()
            self.alive = False
            self.model.hide()
            self.legs.hide()
            self.body = loader.load_model('assets/bam/player_gore.bam')
            self.body.find('**/hair').set_color(COLORS[self.color])
            self.body.reparent_to(render)
            self.body.set_pos(render, self.root.get_pos(render))
            self.body.look_at(d.root)

            self.death_time = 2
            self.fade_card.show()
            self.fade_card.hide(BitMask32.bit([1,0][self.twin]))
            base.task_mgr.add(self.death)
