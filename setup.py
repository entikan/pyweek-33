from setuptools import setup

setup(
    name="dipygun",
    options = {
        'build_apps': {
            'include_patterns': [
                '**/*.conf',
                '**/*.bam',
                '**/*.wav',
                '**/*.ogg',
                '**/*.config'
            ],
            'gui_apps': {
                'dipygun': 'main.py',
            },
            'plugins': [
                'pandagl',
                'p3openal_audio',
            ],
            'platforms': [
                'manylinux1_x86_64',
                'macosx_10_6_x86_64',
                'win_amd64',,
                #'win32',
            ],
        }
    }
)