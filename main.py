import sys
from panda3d.core import CardMaker, LineSegs, CollisionTraverser, DirectionalLight, loadPrcFile
from direct.showbase.ShowBase import ShowBase
from keybindings.device_listener import add_device_listener
from keybindings.device_listener import SinglePlayerAssigner

from game import Game
from game.levels import Level

base = ShowBase()
loadPrcFile("config.prc")
add_device_listener(assigner=SinglePlayerAssigner())
base.cTrav = CollisionTraverser()
base.win.set_clear_color((0,0,0,1))
base.linesegs = LineSegs('lines')
base.linesegs.set_thickness(2)
base.cardmaker = CardMaker('cards')
class Title():
    def __init__(self):
        self.title = loader.load_model('assets/bam/title_screen.bam')
        self.title.reparent_to(render)
        self.title.set_y(15)
        self.title.set_h(-10)
        self.title.set_p(90)
        sun = self.title.attach_new_node(DirectionalLight('sun'))
        sun.set_pos(render, (0,0,0))
        sun.look_at(self.title)
        self.title.set_light(sun)
        self.song = loader.load_sfx('assets/music/lom0.ogg')
        self.song.set_loop(True)
        self.song.play()
        base.task_mgr.add(self.update)


    def update(self, task):
        if base.mouseWatcherNode.is_button_down('w') and base.mouseWatcherNode.is_button_down('arrow_up'):
            self.song.stop()
            self.start()
            return task.done
        return task.cont

    def start(self):
        self.title.detach_node()
        base.game = Game()
        base.game.level = Level(base.game.player_o, base.game.player_b)
        base.game.level.next_level()

base.title = Title()

base.accept('escape', sys.exit)
base.run()
